// MainViewController.swift

import UIKit
import OpenTok
import CoreBluetooth
import QuartzCore

// Replace with your OpenTok API key
var kApiKey = "46307892"
// Replace with your generated session ID
var kSessionId = "2_MX40NjMwNzg5Mn5-MTU1NTIyODc1Mjk2NH5DVEpiOTduaHBqSnM3R3Y3dVdMa3Nlbk5-fg"
// Replace with your generated token
var kToken = "T1==cGFydG5lcl9pZD00NjMwNzg5MiZzaWc9Mzk2ZjkyMzJkZTU0NWVhZTVkMzQ3ZGRlYWYwNDM1ZWY0YmQxN2RjZjpzZXNzaW9uX2lkPTJfTVg0ME5qTXdOemc1TW41LU1UVTFOVEl5T0RjMU1qazJOSDVEVkVwaU9UZHVhSEJxU25NM1IzWTNkVmRNYTNObGJrNS1mZyZjcmVhdGVfdGltZT0xNTU1MjI4Nzc5Jm5vbmNlPTAuNDk5MjQ5MDA0NzUyNzk4OSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNTU3ODIwNzc5JmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9"

/// The option to add a \n or \r or \r\n to the end of the send message
enum MessageOption: Int {
    case noLineEnding,
         newline,
         carriageReturn,
         carriageReturnAndNewline
}

/// The option to add a \n to the end of the received message (to make it more readable)
enum ReceivedMessageOption: Int {
    case none,
         newline
}

final class MainViewController: UIViewController, BluetoothSerialDelegate, OTSessionDelegate, OTPublisherDelegate, OTSubscriberDelegate {

    var session: OTSession?
    var publisher: OTPublisher?
    var publisherMemory: OTPublisher?
    var subscriber: OTSubscriber?
    var pubView: UIView?
    var subView: UIView?
    
//MARK: IBOutlets

    @IBOutlet weak var barButton: UIBarButtonItem!
    @IBOutlet weak var navItem: UINavigationItem!

// Session subscriber functions
    public func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
        print("The subscriber did connect to the stream.")
    }
    
    public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("The subscriber failed to connect to the stream.")
    }
    
// Sessions publisher functions
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("The publisher failed: \(error)")
    }
    
// Sessions delegate functions
    
    func sessionDidConnect(_ session: OTSession) {
        print("The client connected to the OpenTok session.")
        
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        settings.cameraResolution = OTCameraCaptureResolution.high
        
        guard let publisher = OTPublisher(delegate: self, settings: settings) else {
            return
        }
        publisherMemory = publisher
        
        var error: OTError?
        session.publish(publisher, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        guard let publisherView = publisher.view else {
            return
        }
        pubView = publisherView
        let screenBounds = UIScreen.main.bounds
        
        publisherView.frame = CGRect(x: screenBounds.width - 150 - 20, y: screenBounds.height - 150 - 20, width: 150, height: 150)
        /*
        publisherView.frame = CGRect(x: 0, y: 0, width: screenBounds.width, height: screenBounds.height)
        */
        view.addSubview(publisherView)
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("The client disconnected from the OpenTok session.")
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("The client failed to connect to the OpenTok session: \(error).")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("A stream was created in the session.")
        subscriber = OTSubscriber(stream: stream, delegate: self)
        guard let subscriber = subscriber else {
            return
        }
        
        var error: OTError?
        session.subscribe(subscriber, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        guard let subscriberView = subscriber.view else {
            return
        }
        subView = subscriberView
        subscriberView.frame = UIScreen.main.bounds
        view.insertSubview(subscriberView, at: 0)
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("A stream was destroyed in the session.")
    }

// Helpers
    
    func cycleCamera() {
        if (publisherMemory?.cameraPosition == AVCaptureDevicePosition.front) {
            publisherMemory?.cameraPosition = AVCaptureDevicePosition.back
        } else if (publisherMemory?.cameraPosition == AVCaptureDevicePosition.back) {
            publisherMemory?.cameraPosition = AVCaptureDevicePosition.front
        }
    }
    
//MARK: Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // init serial
        serial = BluetoothSerial(delegate: self)
        
        connectToAnOpenTokSession()
        
        
       
    }


    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        UIView.setAnimationsEnabled(false)
        coordinator.notifyWhenInteractionEnds {_ in UIView.setAnimationsEnabled(true)}
        if (subView != nil) {
            subView?.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        }
    }
    
    
    func reloadView() {
        // in case we're the visible view again
        serial.delegate = self
        
        if serial.isReady {
            navItem.title = serial.connectedPeripheral!.name
            barButton.title = "Disconnect"
            barButton.tintColor = UIColor.red
            barButton.isEnabled = true
        } else if serial.centralManager.state == .poweredOn {
            navItem.title = "Ohana"
            barButton.title = "Connect"
            barButton.tintColor = view.tintColor
            barButton.isEnabled = true
        } else {
            navItem.title = "Ohana"
            barButton.title = "Connect"
            barButton.tintColor = view.tintColor
            barButton.isEnabled = false
        }
    }
    
    
    
    func connectToAnOpenTokSession() {
        session = OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)
        var error: OTError?
        session?.connect(withToken: kToken, error: &error)
        if error != nil {
            print(error!)
        }
    }
    

//MARK: BluetoothSerialDelegate
    
    func serialDidReceiveString(_ message: String) {
        
    }
    
    func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
        
    }
    
    func serialDidChangeState() {
        
    }
    
//MARK: IBActions

    @IBAction func barButtonPressed(_ sender: AnyObject) {
        if serial.connectedPeripheral == nil {
            performSegue(withIdentifier: "ShowScanner", sender: self)
        } else {
            serial.disconnect()
            reloadView()
        }
    }
}
