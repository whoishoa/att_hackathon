#include <Servo.h>             //Servo library
#include <SoftwareSerial.h>
 
Servo servo_x;        //initialize a servo object for the connected servo  
Servo servo_y;
Servo servo_z;

int x_pos = 90;
int y_pos = 90;
int z_pos = 90;

SoftwareSerial mySerial(8, 7);
                
int angle = 0;    
 
void setup() 
{ 
  Serial.begin(9600);

  mySerial.begin(9600);

  delay(100);
  Serial.println("Connected correctly...");
  servo_x.attach(9);      // attach the signal pin of servo to pin9 of arduino
  servo_y.attach(10);
  servo_z.attach(11);
}
  
void loop() 
{
  delay(10); //For serial stability.
  String command;
  int amount;
  if (mySerial.available()) {
    command = mySerial.readStringUntil(58); // period (:)
    amount = mySerial.readStringUntil(46).toInt(); // period (.)
    
    Serial.println("Got input:");
    Serial.println(command);
    Serial.println(amount);
    if (command == "dx") { // delta x
      x_pos = x_pos + amount;
      if (x_pos < 0) {
        x_pos = 0;
      }
      if (x_pos > 180) {
        x_pos = 180;
      }
      servo_x.write(x_pos);
    } else if (command == "dy") {
      y_pos = y_pos + amount;
      if (y_pos < 0) {
        y_pos = 0;
      }
      if (y_pos > 180) {
        y_pos = 180;
      }
      servo_y.write(y_pos);
    } else if (command == "dz") {
      z_pos = z_pos + amount;
      if (z_pos < 0) {
        z_pos = 0;
      }
      if (z_pos > 180) {
        z_pos = 180;
      }
      servo_z.write(z_pos);
    } else if (command == "xpos") { // x absolute position
      x_pos = amount;
      if (x_pos < 0) {
        x_pos = 0;
      }
      if (x_pos > 180) {
        x_pos = 180;
      }
      servo_x.write(x_pos);
    } else if (command == "ypos") {
      y_pos = amount;
      if (y_pos < 0) {
        y_pos = 0;
      }
      if (y_pos > 180) {
        y_pos = 180;
      }
      servo_y.write(y_pos);
    } else if (command == "zpos") {
      z_pos = amount;
      if (z_pos < 0) {
        z_pos = 0;
      }
      if (z_pos > 180) {
        z_pos = 180;
      }
      servo_z.write(z_pos);
    } else if (command == "backpack_start") {
      x_pos = 90;
      y_pos = 90;
      z_pos = 90;
      servo_x.write(x_pos);
      servo_y.write(y_pos);
      servo_z.write(z_pos);
    } else if (command == "hat_start") {
      x_pos = 0;
      y_pos = 0;
      z_pos = 0;
      servo_x.write(x_pos);
      servo_y.write(y_pos);
      servo_z.write(z_pos);
    }
  }
   
}
