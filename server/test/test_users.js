// Run on the front-end only
var TestCases = -12;
var Passed = 0;

function assert(test, testval, msg) {
	TestCases ++;
	if (test != testval) {
		console.log("Failed test case #" + TestCases + ":", msg);
	} else {
		Passed ++;
		console.log("Passed test case #" + TestCases);
	}
}

var cemail = "hoa" + Math.random() + "@test.com";

$.post("api/users/", {name: "Hoa Mai", email: cemail}, function(data, err) { // -11
	console.log(data);
	assert(data.message, "Please provide a password", "No password 1 isn't working");

	$.post("api/users/", {name: "Hoa Mai", email: cemail, password: ""}, function(data, err) { // -10
	assert(data.message, "Please provide a password", "No password 2 isn't working");

	$.post("api/users/", {name: "Hoa Mai", email: "test@test.com", password: "  "}, function(data, err) { // -9
	assert(data.message, "An account under this email already exists", "Password valid isn't working");

$.post("api/users/", {name: "Hoa Mai", email: "Kevin Tran", password: "panda"}, function(data, err) { // -8
	assert(data.message, "The email provided is not a valid email", "Invalid isn't working");

$.post("api/users/", {name: "Hoa Mai", password: "panda"}, function(data, err) { // -7
	assert(data.message, "Please provide a email", "No email 1 isn't working");

$.post("api/users/", {name: "Hoa Mai", email: "  \t ", password: "panda"}, function(data, err) { // -6
	assert(data.message, "Please provide a email", "No email 2 isn't working");

$.post("api/users/", {name: "Hoa Mai", email: "", password: "panda"}, function(data, err) { // -5
	assert(data.message, "Please provide a email", "No email 3 isn't working");

$.post("api/users/", {}, function(data, err) { // -4
	assert(data.message, "Please provide a name", "No name 1 isn't working");

$.post("api/users/", {email: "test@test.com", password: "panda"}, function(data, err) { // -3
	assert(data.message, "Please provide a name", "No name 2 isn't working");

$.post("api/users/", {name: "  ", email: "test@test.com", password: "panda"}, function(data, err) { // -2
	assert(data.message, "Please provide a name", "No name 3 isn't working");

$.post("api/users/", {name: "Hoa Mai", email: "test@test.com", password: "panda"}, function(data, err) { // -1
	assert(data.message, "An account under this email already exists", "Email is taken isn't working");

$.post("api/users/", {name: "Hoa Mai", email: cemail, password: "panda"}, function(data, err) { // 0
	assert(data.success, true, "Signup isn't working");

$.post("api/users/logout/", function(data, err) { // 1
	assert(data.success, true, "Logout isn't working");

$.get("api/users/1/", function(data, err) { // 2
	assert(data.message, "No user logged in", "Can access user data while logged out");

$.post("api/users/login/", {email: cemail, password: "panda"}, function(data, err) { // 3
	assert(data.success, true, "Login isn't working");

var currId = data.result.id;

$.get("api/users/" + currId + "/", function(data, err) { // 4
	assert(data.success, true, "Can't access own user data");

$.get("api/users/" + (currId + 1) + "/", function(data, err) { // 5
	assert(data.message, "You do not have access to this user's info", "Can access other user's data");

$.post("api/users/" + currId + "/", {password: "panda"}, function(data, err) { // 6
	assert(data.message, "Please change something", "Edit must have something changed not working");

$.post("api/users/" + currId + "/", {password: "panda", newpassword: "dragons"}, function(data, err) { // 7
	assert(data.success, true, "Can't change passwords");

$.post("api/users/" + currId + "/", {password: "panda", newpassword: "dragons"}, function(data, err) { // 8
	assert(data.success, false, "Password change not applied");

$.post("api/users/" + currId + "/", {password: "dragons", newname: "Sam Free", newemail: "test@test.com"}, function(data, err) { // 9
	assert(data.message, "An account under this email already exists", "Edit taken email broken");

$.post("api/users/" + currId + "/", {password: "dragons", newname: "Sam Free", newemail: "testtest.com"}, function(data, err) { // 10
	assert(data.message, "The email provided is not a valid email", "Edit bad email broken");

$.post("api/users/" + currId + "/", {password: "dragons", newname: "   ", newemail: "\t\r", newpassword: ""}, function(data, err) { // 11
	assert(data.message, "Please change something", "Edit must have something changed 2 not working");

var cemail = "hoa" + Math.random() + "@test.com";

$.post("api/users/" + currId + "/", {password: "dragons", newname: "Sam Free", newemail: cemail}, function(data, err) { // 12
	assert(data.success, true, "Edit not working");

$.get("api/users/" + currId + "/", function(data, err) { // 13
	assert(data.result.name, "Sam Free", "Name change didn't work");

$.post("api/users/logout/", function(data, err) { // 14
	assert(data.success, true, "Logout isn't working");

$.post("api/users/" + currId + "/", {password: "dragons", email:"fate" + Math.random() + "@test.com"}, function(data, err) { // 15
	assert(data.message, "No user logged in", "Can edit user data while logged out");

$.post("api/users/login/", {email: cemail, password: "panda"}, function(data, err) { // 16
	assert(data.success, false, "Old Login is still working");

$.post("api/users/login/", function(data, err) { // 17
	assert(data.message, "Please fill out both the email and password", "New Login isn't working");

$.post("api/users/login/", {email: cemail, password: "dragons"}, function(data, err) { // 18
	assert(data.success, true, "New Login isn't working");



}); // 18
});
}); // 16
});
});
}); 
}); // 12
}); // 11
}); // 10
}); // 9
}); // 8
}); // 7
}); // 6
}); // 5
}); // 4
}); // 3
}); // 2
}); // 1
}); // 0
}); // -1
}); // -2
});
}); // -4
});
}); // -6
});
});
});
}); // -10
});


