var config = require('../config');
var db = require("../db");

// change to 0 to apply all alters
const currentVersion = 4;

if (currentVersion < 1) {

  db.query("ALTER TABLE modes ADD nextmode INTEGER, ADD prevmode INTEGER, ADD localid INTEGER", [], function(err, res) {
    console.log("alter 1", err);
  });

}

if (currentVersion < 2) {

  db.query("ALTER TABLE users ADD nexturl INTEGER REFERENCES images(id) DEFAULT(5)", [], function(err, res) {
    console.log("alter 2", err);
  });

}

if (currentVersion < 3) {

  db.query("ALTER TABLE users ADD age INTEGER, ADD job VARCHAR(100)", [], function(err, res) {
    console.log("alter 3", err);
  });

}

if (currentVersion < 4) {

  db.createTable("action", [
    "id SERIAL PRIMARY KEY",
    "actiontype VARCHAR(254) not null",
    "data VARCHAR(254) not null",
    "time TIMESTAMP",
    "userid INTEGER REFERENCES users (id) NOT NULL"
  ], function(err, res) {
    console.log("alter 4", err);
  });

}