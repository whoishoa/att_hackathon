var config = require('../config');
var db = require("../db");

db.deleteTable(["images", "users", "modes", "bookmarks", "blocks", "tasks", "recovery"], function(err, res) {

	db.createTable("images", [
		"id SERIAL PRIMARY KEY",
		"url VARCHAR(2083)"
	], function(err, res) {
		console.log("done 7", err);

		db.createTable("users", [
			"id SERIAL PRIMARY KEY",
			"name VARCHAR(100) not null",
			"email VARCHAR(254) not null",
			"password VARCHAR(80) not null", // 40 character password limit
			"last_visited TIMESTAMP WITH TIME ZONE not null DEFAULT CURRENT_TIMESTAMP",
			"completed INTEGER not null DEFAULT 0",
			"background VARCHAR(2083)",
			"last_image INTEGER REFERENCES images (id)",
			"UNIQUE (email)"
		], function(err, res) {
			console.log("done 1", err);

			db.addEntry("users", {
				name: "Mai Test",
				email: "test@test.com",
				password: db.encrypt("test"),
				last_visited: ["CURRENT_TIMESTAMP"],
				completed: 0,
				background: "someurl.com"
			}, function(err, res) {
				console.log("add 1", err);
			});

			db.addEntry("users", {
				name: "Second Test",
				email: "test2@test.com",
				password: db.encrypt("test2"),
				last_visited: ["CURRENT_TIMESTAMP"],
				completed: 0,
				background: "someurl.com"
			}, function(err, res) {
				console.log("add 1", err);
			});

			db.createTable("recovery", [
				"email VARCHAR(254) not null",
				"code INTEGER not null",
				"requested TIMESTAMP WITH TIME ZONE not null DEFAULT CURRENT_TIMESTAMP"
			], function(err, res) {
				console.log("done 6", err);
			});

			db.createTable("modes", [
				"id SERIAL PRIMARY KEY",
				"name VARCHAR(40)",
				"userid INTEGER REFERENCES users (id) NOT NULL",
				"imageid INTEGER REFERENCES images (id)",
				"completed INTEGER",
				"UNIQUE (userid, name)"
			], function(err, res) {
				console.log("done 2", err);

				db.query("ALTER TABLE users ADD current_mode INTEGER REFERENCES modes(id), ADD def_free_mode INTEGER REFERENCES modes(id), ADD def_task_mode INTEGER REFERENCES modes(id), ADD def_break_mode INTEGER REFERENCES modes(id)", [], function(err, res) {
    			console.log("alter 1", err);
    		});

				db.createTable("bookmarks", [
					"id SERIAL PRIMARY KEY",
					"label VARCHAR(40)",
					"color VARCHAR(40)",
					"image VARCHAR(2083)",
					"url VARCHAR(2083)",
					"mode INTEGER REFERENCES modes (id) NOT NULL"
				], function(err, res) {
					console.log("done 3", err);
				});

				db.createTable("blocks", [
					"id SERIAL PRIMARY KEY",
					"url VARCHAR(2083) not null",
					"mode INTEGER REFERENCES modes (id) NOT NULL"
				], function(err, res) {
					console.log("done 4", err);
				});

				db.createTable("tasks", [
					"id SERIAL PRIMARY KEY",
					"name VARCHAR(254) not null",
					"status SMALLINT",
					"completed TIMESTAMP",
					"time_taken INTEGER",
					"modeid INTEGER REFERENCES modes (id) NOT NULL",
					"userid INTEGER REFERENCES users (id) NOT NULL"
				], function(err, res) {
					console.log("done 5", err);
				});
			});
			
		}); // end of create users

		var images = require("./images");
		
	}); // end of create images

}); // end of delete tables




/*
var conString = "pg://" + config.DATABASE_USER + ":" + config.DATABASE_PASSWORD + "@" + config.DATABASE_HOST + ":" + config.DATABASE_PORT + "/" + config.DATABASE_NAME;

var client = new pg.Client(conString);
client.connect();

var query = client.query('CREATE TABLE fire2(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
query.on('end', function() { 

	client.query('CREATE TABLE water2(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');

});
*/