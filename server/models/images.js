var db = require("../db");
var config = require('../config');

var imgList = [];

var version = 0;

if (version < 1) {
  imgList = imgList.concat(config.IMAGES);
  imgList = imgList.concat([
  	"imgs/wallpaper4.jpg",
  	"imgs/wallpaper.jpg"
  ]);
}

if (version < 2) {
  imgList = imgList.concat([
    "http://backgroundcheckall.com/wp-content/uploads/2017/12/scenic-background-11.jpg",
    "https://wallpapermemory.com/uploads/175/scenic-background-hd-1080p-258971.jpg",
    "https://www.scenicworld.com.au/wp-content/uploads/2012/09/SW-inside-0051.jpg"
  ]);
}

if (version < 3) {
  imgList = imgList.concat([
    "https://farm2.staticflickr.com/1845/30734653318_cf94ed6802_o.jpg",
    "https://farm2.staticflickr.com/1870/43695823745_404c483501_o.jpg",
    "https://c2.staticflickr.com/2/1839/43305324524_762722afbe_o.jpg",
    "https://c2.staticflickr.com/2/1814/43975459582_f0a6b40a9a_o.jpg",
    "https://farm2.staticflickr.com/1850/43695824425_259589028a_o.jpg",
    "https://c2.staticflickr.com/2/1798/44024173651_6c5f299f16_o.jpg",
    "https://farm2.staticflickr.com/1854/43695824745_fa6caa716c_o.jpg",
    "https://c2.staticflickr.com/2/1775/30155532978_d0093dc9ee_o.jpg",
    "https://farm2.staticflickr.com/1872/43886598874_f093182089_o.jpg",
    "https://farm2.staticflickr.com/1846/43886597284_c98c9dea22_o.jpg",
    "https://c2.staticflickr.com/2/1833/30155534738_5de347de4d_o.jpg",
    "https://c2.staticflickr.com/2/1834/44024176431_6c1d8222e1_o.jpg",
    "https://farm2.staticflickr.com/1899/43886588774_b24d81bc86_o.jpg",
    "https://c2.staticflickr.com/2/1793/44024178481_486cfd8e6d_o.jpg",
    "https://c2.staticflickr.com/2/1775/30155539268_352dc11c88_o.jpg",
    "https://c2.staticflickr.com/2/1798/44024179311_c8d3ac1739_o.jpg",
    "https://c2.staticflickr.com/2/1792/42214921090_0423c5dc71_o.jpg",
    "https://farm2.staticflickr.com/1854/43886590724_006ec698e0_o.jpg",
    "https://farm2.staticflickr.com/1884/43886593184_0faba441bc_o.jpg",
    "https://c2.staticflickr.com/2/1812/44024182391_d52a1499a1_o.jpg",
    "https://farm2.staticflickr.com/1870/29667373697_deda449198_o.jpg",
    "https://c2.staticflickr.com/2/1779/42214540230_7102f4a363_o.jpg",
    "https://c2.staticflickr.com/2/1817/43975021372_9c5c83afc6_o.jpg",
    "https://c2.staticflickr.com/2/1771/42214543940_f7fbd9eb9f_o.jpg",
    "https://c2.staticflickr.com/2/1797/43975031602_3e0a07cfdd_o.jpg",
    "https://c2.staticflickr.com/2/1791/43975032312_838624031d_o.jpg",
    "https://c2.staticflickr.com/2/1776/30155092588_aec3afe76e_o.jpg",
    "https://c2.staticflickr.com/2/1840/43117563405_c9a3ca7713_o.jpg",
    "https://farm2.staticflickr.com/1873/29667374957_92c499d963_o.jpg",
    "https://farm2.staticflickr.com/1851/43695836565_2df863c77e_o.jpg",
    "https://c2.staticflickr.com/2/1792/43117568415_4df99363e8_o.jpg",
    "https://c2.staticflickr.com/2/1792/43117570855_b07a48ee31_o.jpg",
    "https://c2.staticflickr.com/2/1794/42214555960_41f0d77a55_o.jpg",
    "https://c2.staticflickr.com/2/1777/42214557160_5aa6c7b5ab_o.jpg",
    "https://farm2.staticflickr.com/1884/43695836835_0921ff4033_o.jpg",
    "https://c2.staticflickr.com/2/1795/42214561140_3daec517f0_o.jpg",
    "https://c1.staticflickr.com/1/930/42214563570_4cc17eea43_o.jpg",
    "https://farm2.staticflickr.com/1895/43695837235_b1d0969098_o.jpg",
    "https://c2.staticflickr.com/2/1775/29086077257_36bc8d7cbb_o.jpg",
    "https://c2.staticflickr.com/2/1798/43117583965_3a5a8fe8c7_o.jpg",
    "https://c2.staticflickr.com/2/1798/29086085047_6713c7a7bb_o.jpg",
    "https://farm2.staticflickr.com/1847/43695840285_be35facf02_o.jpg",
    "https://farm2.staticflickr.com/1854/43695841735_f060c661c0_o.jpg",
    "https://farm2.staticflickr.com/1870/43695842715_8814e6640f_o.jpg"
  ]);
}

var x = 1;
var entries = [];
for (var i = 0; i < imgList.length; i++) {
	entries.push({
		url: imgList[i],
	});
}

db.addEntry("images", entries, function(err, res) {
	console.log("images " + (x++), err);
});