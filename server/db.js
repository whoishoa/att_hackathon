var config = require('./config');
var pg = require('pg');
var crypto = require('crypto');

var cryptoSalt = "";

var conString = "pg://" + config.DATABASE_USER + ":" + config.DATABASE_PASSWORD + "@" + config.DATABASE_HOST + ":" + config.DATABASE_PORT + "/" + config.DATABASE_NAME;

var db_query = function(text, values, cb) {
  pg.connect(conString, function(err, client, done) {
    client.query(text, values, function(err, result) {
      done();
      if (cb) {
      	cb(err, result);
      }
    })
  });
}

module.exports = {
	pg: pg,
	conString: conString,
  query: db_query,
  createTable: function(tableName, tableList, cb) {
  	var text = "CREATE TABLE " + tableName + "(" + tableList.join(", ") + ")";
  	db_query(text, [], cb);
  }, 
  addEntry: function(tableName, entries, cb) {
    var multi = false;
    if (Object.prototype.toString.call( entries ) == "[object Array]") {
      multi = true;
    }
    var keys = Object.keys( (multi) ? entries[0] : entries );
    var values = [];
    var entryStrings = [];
    var cnt = 0;
    for (var j = 0; (j == 0) || (multi && j < entries.length); j++) {
      var entry = (multi) ? entries[j] : entries;
      var valuesIndex = [];
      for (var i = 0; i < keys.length; i++) {
        if (Object.prototype.toString.call( entry[keys[i]] ) == "[object Array]") {
          valuesIndex.push(entry[keys[i]]);
          cnt ++;
        } else {
          values.push(entry[keys[i]]);
          valuesIndex.push("$" + (j * keys.length + i + 1 - cnt));
        }
      }
      entryStrings.push("(" + valuesIndex.join(", ") + ")");
    }
    var text = "INSERT INTO " + tableName + " (" + keys.join(", ") + ") VALUES " + entryStrings.join(", ") + " RETURNING *";
    db_query(text, values, cb);
  },
  updateEntry: function(tableName, id, entry, cb) {
  	var keys = Object.keys(entry);
  	var values = [];
  	var valuesIndex = [];
  	var cnt = 0;
  	for (var i = 0; i < keys.length; i++) {
  		if (Object.prototype.toString.call( entry[keys[i]] ) == "[object Array]") {
  			valuesIndex.push(entry[keys[i]]);
  			cnt ++;
  		} else {
  			values.push(entry[keys[i]]);
  			valuesIndex.push(keys[i] + " = $" + (i + 1 - cnt));
  		}
  	}
  	db_query("UPDATE " + tableName + " SET " + valuesIndex.join(", ") + " WHERE id = " + id, values, cb);
  },
  deleteTable: function(tableList, cb) {
  	var text = "DROP TABLE " + tableList.join(", ");
  	db_query(text, [], cb);
  }, 
  encrypt: function(text) {
	  var cipher = crypto.createCipher('aes-256-ctr', text + config.CRYPTO_SALT)
	  var crypted = cipher.update(text,'utf8','hex')
	  crypted += cipher.final('hex');
	  return crypted;
	},
	decrypt: function(text, password) {
		var decipher = crypto.createDecipher('aes-256-ctr', password + config.CRYPTO_SALT)
	  var dec = decipher.update(text,'hex','utf8')
	  dec += decipher.final('utf8');
	  return dec;
	}
}