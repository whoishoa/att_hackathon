var db = require("../db");
var library = require("../library");
var express = require('express');
var config = require('../config');

// helper method for exports.edit
function manageNewUserChanges(req, toManage) {
  if (req.body.newpassword) {
    toManage.password = db.encrypt(req.body.newpassword);
  } 
  if (req.body.newname) {
    toManage.name = req.body.newname.trim();
  }
  if (req.body.newemail) {
    toManage.email = req.body.newemail.trim();
  }
}

function parseUserObj(userObj) {
  userObj = library.clone(userObj);
  delete userObj.password;
  delete userObj.last_visited;
  return userObj;
}

module.exports = function(app) {
  return {
    isLoggedIn: function(req, res, next) {
      console.log("boo");
      if (req.session.user) {
        next();
      } else {
        res.json({success: false, message: "No user logged in"});
      }
    },

    main: function(req, res) {
      library.getIP(function(err, ip) {
        res.render('main', {
          env: process.env.NODE_ENV,
          ip: ip
        });
      });
      
    },

    goals: function(req, res) {
      library.getIP(function(err, ip) {
        res.render('goals', {
          env: process.env.NODE_ENV,
          ip: ip
        });
      });
    },

    privacy: function(req, res) {
      library.getIP(function(err, ip) {
        res.render('privacy', { });
      });
    },

    terms: function(req, res) {
      library.getIP(function(err, ip) {
        res.render('terms', { });
      });
    },

    action: function(req, res) {
      if (msg = library.standardStringValidate(req.body.actiontype, 100, "actiontype", true)) {
        res.json({success: false, message: msg, source: ["actiontype"]});
      } else if (msg = library.standardStringValidate(req.body.data, 254, "data")) {
        res.json({success: false, message: msg, source: ["data"]});
      } else {
        db.addEntry("action", {
          actiontype: req.body.actiontype.trim(),
          data: req.body.data.trim(),
          time: ["CURRENT_TIMESTAMP"],
          userid: req.body.userid
        }, function(err, result) {
          if (err) {
            res.json({success: false, message: "Something went wrong" + err});
          } else {
            res.json({success: true, message: "Action recorded"});
          }
        });
      }
    },

    add2: function(req, res) {
      var msg;

      if (msg = library.standardStringValidate(req.body.name, 100, "name", true)) {
        res.json({success: false, message: msg, source: ["name"]});
      } else if (msg = library.standardEmailValidate(req.body.email, 254, "email")) {
        res.json({success: false, message: msg, source: ["email"]});
      } else if (msg = library.standardStringValidate(req.body.age + "", 100, "age", true)) {
        res.json({success: false, message: msg, source: ["age"]});
      } else if (msg = library.standardStringValidate(req.body.job, 100, "occupation", true)) {
        res.json({success: false, message: msg, source: ["occupation"]});
      } else {
        db.addEntry("users", {
          name: req.body.name.trim(),
          email: req.body.email.trim(),
          age: Number.parseInt(req.body.age),
          job: req.body.job.trim(),
          password: db.encrypt("default"),
          last_visited: ["CURRENT_TIMESTAMP"],
          completed: 0,
          background: "none",
          last_image: 3
        }, function(err, result) {
          if (err) {
            db.query("SELECT * FROM users WHERE email = $1",
              [req.body.email], function(err, result) {
                if (err) {
                  console.log("An ERROR in exports.login occurred: ", err);
                  res.json({success: false, message: "Something went wrong"});
                } else {
                  if (result.rowCount == 0) {
                    res.json({success: false, message: "No account has been found with this email.", source: ["email"]});
                  } else {
                    var userObj = result.rows[0];
                    db.query("SELECT modes.*, images.url FROM modes LEFT JOIN images ON images.id = modes.imageid WHERE userid = " + userObj.id, [], 
                      function(err, mode_results) {

                      if (err) {
                        console.log("An ERROR in exports.login occurred [mode]: ", err);
                        res.json({success: false, message: "Sorry, we could not log you in at this time."});
                      } else {
                        req.session.user = userObj;
                        userObj = parseUserObj(userObj);
                        res.json({success: true, message: "Account created!", result: userObj, mresult: mode_results.rows});
                      }
                    });
                    
                  }
                }
              }
            );
          } else if (result.rowCount == 1) {
            db.addEntry("modes", [{
              name: "Free Mode",
              userid: result.rows[0].id,
              imageid: 1,
              completed: 0
            }, {
              name: "Task Mode",
              userid: result.rows[0].id,
              imageid: 2,
              completed: 0
            }], function(err, mode_results) {
              var mresults = mode_results.rows;
              for (var i = 0; i < mresults.length; i++) {
                mresults[i].url = config.IMAGES[mresults[i].imageid - 1];
              }
              result.rows[0].current_mode = mresults[0].id;
              
              var newUserData = {
                current_mode: mresults[0].id, 
                def_free_mode: mresults[0].id,
                def_task_mode: mresults[1].id,
                def_break_mode: mresults[1].id
              };
              db.updateEntry("users", result.rows[0].id, newUserData);
              req.session.user = library.mergeObjects(result.rows[0], newUserData);

              app.render('welcomeEmail', {user: req.session.user}, function(err, html) {
                library.sendEmail(req.session.user.email, "Welcome to Moderamic!", html, html, function(err) {
                  if (err) {
                    console.log(err);
                  }
                });
                res.json({success: true, message: "Account created!", 
                  mresult: mresults,
                  result: parseUserObj(req.session.user)});
              });
            })

          } else {
            res.json({success: false, message: "Something went wrong"});
          }
        });
      }
    },

    /*
     * req.body args: name, email, password
     */
    add: function(req, res) {
      var msg;
      if (msg = library.standardStringValidate(req.body.name, 100, "name", true)) {
        res.json({success: false, message: msg, source: ["name"]});
      } else if (msg = library.standardEmailValidate(req.body.email, 254, "email")) {
        res.json({success: false, message: msg, source: ["email"]});
      } else if (msg = library.standardStringValidate(req.body.password, 40, "password")) {
        res.json({success: false, message: msg, source: ["password"]});
      } else {
        db.addEntry("users", {
          name: req.body.name.trim(),
          email: req.body.email.trim(),
          password: db.encrypt(req.body.password),
          last_visited: ["CURRENT_TIMESTAMP"],
          completed: 0,
          background: "none",
          last_image: 3
        }, function(err, result) {
          if (err) {
            console.log("An ERROR in exports.add occurred: ", err);
            res.json({success: false, message: "An account under this email already exists", source: ["email"]});
          } else if (result.rowCount == 1) {
            db.addEntry("modes", [{
              name: "Free Mode",
              userid: result.rows[0].id,
              imageid: 1,
              completed: 0
            }, {
              name: "Task Mode",
              userid: result.rows[0].id,
              imageid: 2,
              completed: 0
            }], function(err, mode_results) {
              var mresults = mode_results.rows;
              for (var i = 0; i < mresults.length; i++) {
                mresults[i].url = config.IMAGES[mresults[i].imageid - 1];
              }
              result.rows[0].current_mode = mresults[0].id;
              
              var newUserData = {
                current_mode: mresults[0].id, 
                def_free_mode: mresults[0].id,
                def_task_mode: mresults[1].id,
                def_break_mode: mresults[1].id
              };
              db.updateEntry("users", result.rows[0].id, newUserData);
              req.session.user = library.mergeObjects(result.rows[0], newUserData);

              app.render('welcomeEmail', {user: req.session.user}, function(err, html) {
                library.sendEmail(req.session.user.email, "Welcome to Moderamic!", html, html, function(err) {
                  if (err) {
                    console.log(err);
                  }
                });
                res.json({success: true, message: "Account created!", 
                  mresult: mresults,
                  result: parseUserObj(req.session.user)});
              });
            })

          } else {
            res.json({success: false, message: "Something went wrong"});
          }
        });
      }
    },

    /*
     * req.params args: user_id
     * req.body args: password
     * req.body optionals: newpassword, newname, newemail
     */
    edit: function(req, res) {
      var msg;
      if (req.session.user.id != req.params.user_id) {
        res.json({success: false, message: "You do not have access to edit this user's info"});
      } else if (!req.body.password || req.session.user.password != db.encrypt(req.body.password)) {
        console.log(req.session.user);
        console.log(db.encrypt(req.body.password));
        res.json({success: false, message: "Incorrect password"});
      } else if (library.isNotProvided(req.body.newpassword) && library.isNotProvided(req.body.newname, true) && library.isNotProvided(req.body.newemail, true)) {
        res.json({success: false, message: "Please change something"});
      } else if (msg = library.standardStringValidate(req.body.newname, 100, "name", true, true)) {
        res.json({success: false, message: msg});
      } else if (msg = library.standardEmailValidate(req.body.newemail, 254, "email", true)) {
        res.json({success: false, message: msg});
      } else if (msg = library.standardStringValidate(req.body.newpassword, 40, "password", false, true)) {
        res.json({success: false, message: msg});
      } else {
        var changed = {};
        manageNewUserChanges(req, changed);
        db.updateEntry("users", req.params.user_id, changed, function(err, result) {
          if (err) {
            console.log("An ERROR in exports.edit occurred: ", err);
            res.json({success: false, message: "An account under this email already exists"});
          } else if (result.rowCount == 1){
            manageNewUserChanges(req, req.session.user);
            res.json({success: true, message: "Changes has been saved!"});
          } else {
            res.json({success: false, message: "This user has been deleted"});
          }
        });
      }
    },

    /*
     * req.body args: email, password
     */
    login: function(req, res) {
      if (library.isNotProvided(req.body.password) && library.isNotProvided(req.body.email, true)) {
        res.json({success: false, message: "Please fill out both the email and password", source: ["email", "password"]});
      } else {
        var pass = req.body.password;
        db.query("SELECT * FROM users WHERE email = $1",
          [req.body.email], function(err, result) {
            if (err) {
              console.log("An ERROR in exports.login occurred: ", err);
              res.json({success: false, message: "Something went wrong"});
            } else {
              if (result.rowCount == 0) {
                res.json({success: false, message: "No account has been found with this email.", source: ["email"]});
              } else {
                var userObj = result.rows[0];
                if (userObj.password != db.encrypt(pass)) {
                  res.json({success: false, message: "Incorrect password", source: ["password"]});
                } else {
                  db.query("SELECT modes.*, images.url FROM modes LEFT JOIN images ON images.id = modes.imageid WHERE userid = " + userObj.id, [], 
                    function(err, mode_results) {

                    if (err) {
                      console.log("An ERROR in exports.login occurred [mode]: ", err);
                      res.json({success: false, message: "Sorry, we could not log you in at this time."});
                    } else {
                      req.session.user = userObj;
                      userObj = parseUserObj(userObj);
                      res.json({success: true, result: userObj, mresult: mode_results.rows});
                    }
                  });
                }
              }
            }
          }
        );
      }
    },

    // No args
    logout: function(req, res) {
      req.session.regenerate(function(err) {
        res.json({success: true, message: "Logged out successfully!"});
      });
    },

    /*
     * req.params args: user_id
     */
    get: function(req, res) {
      if (req.session.user.id != req.params.user_id) {
        res.json({success: false, message: "You do not have access to this user's info"});
      } else {
        db.query("SELECT * FROM users where id = " + req.params.user_id, 
          [], function(err, result) {
            if (err) {
              console.log("An ERROR in exports.get occurred: ", err);
              res.json({success: false, message: "Something went wrong"});
            } else {
              if (result.rowCount == 0) {
                res.json({success: false, message: "No user found"});
              } else {
                req.session.user = result.rows[0];
                res.json({success: true, result: parseUserObj(result.rows[0])});
              }
            }
          }
        );
      }
    },

    /*
     * req.params args: email
     */
    recover: function(req, res) {
      var msg;
      if (msg = library.standardEmailValidate(req.body.newemail, 254, "email", true)) {
        res.json({success: false, message: msg, source: ["email"]});
      } else {
        db.query("SELECT * FROM users WHERE email = $1",
          [req.body.email], function(err, result) {
            if (err) {
              console.log("An ERROR in exports.login occurred: ", err);
              res.json({success: false, message: "Something went wrong"});
            } else {
              if (result.rowCount == 0) {
                res.json({success: false, message: "No account has been found with this email.", source: ["email"]});
              } else {
                var recoveryCode = Math.floor(Math.random() * 1000000);
                var user = result.rows[0];

                db.query("DELETE FROM recovery WHERE email = $1", [req.body.email], function(err, drop_result) {
                  if (err) {
                    console.log("An ERROR in exports.login occurred (DROP): ", err);
                    res.json({success: false, message: "Something went wrong. Try again later."});
                  } else {
                    db.addEntry("recovery", {
                      email: req.body.email,
                      code: recoveryCode,
                      requested: ["CURRENT_TIMESTAMP"],
                    }, function(err, recovery_result) {
                      if (err) {
                        console.log("An ERROR in exports.login occurred: ", err);
                        res.json({success: false, message: "Cannot create a recovery code at this time. Try again later."});
                      } else {
                        app.render('recoveryEmail', {user: user, code: recoveryCode}, function(err, html) {
                          library.sendEmail(req.body.email, "Recover Your Task Mode Account", html, html, function(err) {
                            if (err) {
                              console.log(err);
                            }
                          });
                          res.json({success: true, message: "The recovery code has been sent to your email."});
                        });
                      }
                    });
                  }
                })
              }
            }
          }
        );
      }
    },

    background: function(req, res) {
      const imgFetch = () => {
        db.query("SELECT * FROM images WHERE id = (SELECT nexturl FROM users WHERE id = $1)", [req.session.user.id], function(err, result) {
          if (err) {
            res.json({success: false, message: "Something went wrong. Try again later." + err});
          } else if (result.rows.length == 0) {
            res.json({success: false, message: msg, source: ["nexturl"]});
          } else {
            res.json(result.rows);
          }
        });
      }

      if (req.query.increment) {
        db.query("UPDATE users SET nexturl = nexturl + 1 WHERE id = $1", [req.session.user.id], function(err, result) {
          if (err) {
            res.json({success: false, message: "Something went wrong. Try again later." + err});
          } else {
            imgFetch(); 
          }
        });
      } else {
        imgFetch();
      }
    },

    /* 
     * req.params args: email, code, password
     */
    recover2: function(req, res) {
      if (msg = library.standardEmailValidate(req.body.email, 254, "email", true)) {
        res.json({success: false, message: msg, source: ["email"]});
      } else if (msg = library.standardIntValidate(req.body.code, "recovery code")) {
        res.json({success: false, message: msg, source: ["password"]});
      } else if (msg = library.standardStringValidate(req.body.password, 40, "password")) {
        res.json({success: false, message: msg, source: ["password"]});
      } else {
        db.query("SELECT * FROM recovery WHERE email = $1", [req.body.email], function(err, result) {
          if (err) {
            console.log("An ERROR in exports.recover2 occurred: ", err);
            res.json({success: false, message: "Something went wrong"});
          } else {
            if (result.rows.length == 0) {
              res.json({success: false, message: "No recovery attempt has been found. Please request a recovery code.", source: ["code"]});
            } else if (result.rows[0].code == req.body.code) {
              if (((new Date()) - result.rows[0].requested) / 1000 < 15 * 60 * 1000) { // 15 minutes
                db.query("UPDATE users SET password = $1 WHERE email = $2 RETURNING *", 
                  [db.encrypt(req.body.password), req.body.email], 
                  function (err, result) {
                    if (err) {
                      console.log("An ERROR in exports.recover2 occurred [after pw change]: ", err);
                      res.json({success: false, message: "Password could not be changed for the time being. Try again later."});
                    } else if (result.rows.length == 0) {
                      res.json({success: false, message: "Could not change password. Please contact support@moderamic.com"});
                    } else {
                      db.query("DELETE FROM recovery WHERE email = $1", [req.body.email]);
                      req.session.user = result.rows[0];
                      res.json({success: true, message: "Your password has successfully been changed!", result: parseUserObj(result.rows[0])});
                    }
                  });
              } else {
                res.json({success: false, message: "The recovery code has expired. Please request a new one.", source: ["code"]});
              }
            } else {
              res.json({success: false, message: "The recovery code is incorrect.", source: ["code"]});
            }
          }
        });
      }
    }

  } // end of return {
}



