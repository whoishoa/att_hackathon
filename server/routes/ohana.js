var db = require("../db");
var library = require("../library");
var express = require('express');
var config = require('../config');

module.exports = function(app) {
  return {
    
    main: function(req, res) {
      library.getIP(function(err, ip) {
        res.render('ohana', {
          env: process.env.NODE_ENV,
          ip: ip
        });
      });
      
    },

    rotate: function(req, res) {
      db.addEntry("ohana_actions", {
          action_type: req.body.direction,
          amount: 0
        }, function(err, result) {
          if (err) {
            res.json({success: false, message: "Something went wrong"});
          } else {
            res.json({success: true, result: result});
          }
        }
      );
    },

    flip: function(req, res) {
      db.addEntry("ohana_actions", {
          action_type: "flip",
          amount: 0
        }, function(err, result) {
          if (err) {
            res.json({success: false, message: "Something went wrong"});
          } else {
            res.json({success: true, result: result});
          }
        }
      );
    },

    actions: function(req, res) {

      db.query("SELECT * FROM keys_pairs WHERE key_string = 'last_ohana_actions'",
        [], function(err, result) {
          if (err) {
            res.json({success: false, message: "Something went wrong"});
          } else {
            if (result.rowCount == 0) {
              res.json({success: false, message: "last_ohana_actions is missing.", source: ["database"]});
            } else {
              var userObj = result.rows[0];
              db.query("SELECT * FROM ohana_actions WHERE id > " + userObj.pair_value + " ORDER BY id", 
                  [], 
                  function(err, mode_results) {
                if (err) {
                  res.json({success: false, message: "Something went wrong"});
                } else {
                  if (mode_results.rows.length) {
                    console.log("hi" + mode_results.rows[mode_results.rows.length - 1].id);
                    db.updateEntry("keys_pairs", userObj.id, {
                      key_string: "last_ohana_actions",
                      pair_value: mode_results.rows[mode_results.rows.length - 1].id
                    }, function(err, nresult) {
                      if (err) {
                        res.json({success: false, message: "Update failed"});
                      } else {
                        res.json({success: true, message: "Success", result: mode_results.rows});
                      }
                    });
                  } else {
                    res.json({success: true, message: "Success", result: mode_results.rows});
                  }
                }
              });
              
            }
          }
        }
      );
    }

  };
}



