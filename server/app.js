var config = require('./config');
var express = require('express');

var http = require("http");
var fs = require('fs');
var path = require('path');

var handlebars = require('express3-handlebars');
var db = require('./db');
var session = express.session;
var pgSession = require('connect-pg-simple')(session);

var log = function(entry) {
  fs.appendFileSync('/tmp/sample-app.log', new Date().toISOString() + ' - ' + entry + '\n');
};

// Create the server instance
var app = express();

// Print logs to the console and compress pages we send
app.use(express.logger());
app.use(express.compress());

app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());

// support sessions
app.use(express.cookieParser());
app.use(session({
  store: new pgSession({
    pg : db.pg, // Use global pg-module
    conString : db.conString // Connect using something else than default DATABASE_URL env variable
  }),
	secret: config.SESSION_SECRET,
  resave: false,
  cookie: { maxAge: 10 * 12 * 30 * 24 * 60 * 60 * 1000 } // 10 years
}));

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
} 

function ensureSecure(req, res, next){
  const xfp =
  req.headers["X-Forwarded-Proto"] || req.headers["x-forwarded-proto"];
  if (xfp === "http" && process.env.NODE_ENV !== "development") {
    res.redirect(301, 'https://' + req.host + req.url);
  } else {
    next();
  }
}

app.all('*', ensureSecure);

var users = require('./routes/users')(app);

var ohana = require('./routes/ohana')(app);


app.get('/', users.main);

app.get('/goals', users.goals);

app.get('/nikko/privacy', users.privacy);
app.get('/nikko/terms', users.terms);

app.get('/re560ler.htm', function(req, res) {
  res.render('temp');
});

app.get('/ohana', ohana.main);

app.post('/ohana/flip', ohana.flip);
app.post('/ohana/rotate', ohana.rotate);

app.get('/ohana/actions', ohana.actions);
app.post('/ohana/actions', ohana.actions);

app.post('/api/users/', users.add); // add a new user
app.post('/api/users2/', users.add2);
app.post('/api/action/', users.action);

app.get('/api/bookmarks/');
app.post('/api/bookmarks/');

app.post('/api/users/login/', users.login);
app.post('/api/users/logout/', users.logout);
app.post('/api/users/recover/', users.recover);
app.post('/api/users/recover2/', users.recover2);
app.get('/api/users/background/', users.isLoggedIn, users.background);

app.post('/api/users/:user_id/', users.isLoggedIn, users.edit); // edit a user

app.get('/api/users/:user_id/', users.isLoggedIn, users.get); // get data about a user

// Start the server
app.set('port', process.env.PORT || 3000);



http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


