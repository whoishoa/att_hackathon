var config = require("./config");
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    transport: 'ses', // loads nodemailer-ses-transport
    accessKeyId: config.AWS_ACCESS_KEY,
    secretAccessKey: config.AWS_SECRET_ACCESS_KEY
});

var net = require('net');
function getNetworkIP(callback) {
  var socket = net.createConnection(80, 'www.google.com');
  socket.on('connect', function() {
    callback(undefined, socket.address().address);
    socket.end();
  });
  socket.on('error', function(e) {
    callback(e, 'error');
  });
}

function emptyTest(test, toTrim) {
	return !test || ((toTrim) ? test.trim() == "" : test == "");
}

function stdIntValidate(test, title, optional) {
	if (emptyTest(test, false)) {
		if (optional) {
			return false;
		}
		return "Please provide a " + title;
	}
}

function stdStringValidate(test, maxLength, title, toTrim, optional) {
	if (emptyTest(test, toTrim)) {
		if (optional) {
			return false;
		}
		return "Please provide a " + title;
	} else if (( (toTrim) ? test.trim() : test ).length > maxLength) {
		return "Your " + title + " must be less than " + maxLength + " characters";
	} else {
		return false;
	}
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}



module.exports = {
	mergeObjects: function(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
	},
	clone: function(obj) { // kinda inefficient, make more efficent later
	  return JSON.parse(JSON.stringify(obj));
	},
	isNotProvided: emptyTest,
	standardIntValidate: stdIntValidate,
	standardStringValidate: stdStringValidate,
	standardEmailValidate: function(test, maxLength, title, optional) {
		var msg;
		if (optional && emptyTest(test, true)) {
			return false;
		} else if (msg = stdStringValidate(test, maxLength, title, true)) {
			return msg;
		} else {
			if (!validateEmail(test.trim())) {
				return "The " + title + " provided is not a valid email";
			} else {
				return false;
			}
		}
	},
	sendEmail: function(email, title, plainMessage, htmlMessage, cb) {
		var mailOptions = {
	    from: 'Moderamic <support@moderamic.com>',
	    to: email,
	    subject: title,
	    text: plainMessage, // plaintext body
	    html: htmlMessage // html body
		};

		transporter.sendMail(mailOptions, cb);
	},
	getIP: getNetworkIP
}
